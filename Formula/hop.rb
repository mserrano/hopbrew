class Hop < Formula
  desc "Multitier Web Programming Environment"
  homepage "http://hop.inria.fr/"
  url "ftp://ftp-sop.inria.fr/indes/fp/Hop/hop-3.3.0.tar.gz"
  version "3.3.0"
  sha256 "e532db0e3aecc4d2a43f1eecf458c1eeeee8aa97c542acbb6dbf245e614cee91"
  revision 1

  depends_on "autoconf" => :build
  depends_on "automake" => :build
  depends_on "libtool" => :build
  depends_on "bigloo-unstable" => :build

  depends_on "bigloo-unstable"
  depends_on "gmp"
  depends_on "pcre"
  depends_on "libunistring"
  depends_on "openssl@1.1"

  def install
    args = %W[
      --prefix=#{prefix}
    ]

    ENV['PKG_CONFIG_PATH'] = "/usr/local/opt/openssl@1.1/lib/pkgconfig"
    system "./configure", *args

    system "make"
    system "make", "install"

    # Install the other manpages too
    manpages = %w[hop]
    manpages.each { |m| man1.install "etc/#{m}.man" => "#{m}.1" }
  end

  test do
    program = <<~EOS
      console.log( "Hello World!" );
      process.exit( 0 );
    EOS
    assert_match "Hello World!\n", pipe_output("#{bin}/hop -j <", program)
  end
end
