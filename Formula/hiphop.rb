class Hiphop < Formula
  desc "Reactive Web Programming Language"
  homepage "http://hop.inria.fr/hiphop"
  url "ftp://ftp-sop.inria.fr/indes/fp/HipHop/hiphop-1.0.0.tar.gz"
  version "1.0.0"
  sha256 "3ba920b9463d6d4987d10782153cd808a078a622ab53959d2d2b99ef9b96c9fd"
  revision 1

  depends_on "bigloo-unstable" => :build
  depends_on "hop" => :build

  depends_on "bigloo-unstable"
  depends_on "hop"

  def install
    args = %W[
      --prefix=#{prefix}
    ]

    ENV['PKG_CONFIG_PATH'] = "/usr/local/opt/openssl@1.1/lib/pkgconfig"
    system "./configure", *args

    system "make"
    system "make", "install-hiphop"
    print "Install locally with \"(mkdir -p $HOME/.node_modules; cd $HOME/.node_modules; ln -s #{prefix}/lib/hiphop/1.0.0 hiphop)\"\n"
  end

  test do
    program = <<~EOS
      console.log( "Hello World!" );
      process.exit( 0 );
    EOS
    assert_match "Hello World!\n", pipe_output("#{bin}/hop -j <", program)
  end
end
