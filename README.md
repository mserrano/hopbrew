Hop Brew Tap
============

Hop formulae for the Homebrew package mangages dedicated to 
[Hop](http://hop/inria.fr) and its applications. This can be used
to install Hop on Mac, Linux, and Windows 10 Subsustem for Linux.


How to use this tap?
--------------------

Just `brew tap hopbrew/hop https://gitlab.inria.fr/mserrano/hopbrew.git`


How to install these formulae?
------------------------------

Just `brew install hopbrew/hop/<formula>`. 
